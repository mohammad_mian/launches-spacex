import { cleanup, screen, act, render, waitFor } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import { mswServer } from '../mocks/mswServer';
import HomePage from '../pages';

describe('When initializing the app.', () => {
  beforeAll(async () => {
    mswServer.listen();
  });

  beforeEach(async () => {
    render(<HomePage />);
    await act(() => Promise.resolve());
  });

  afterEach(() => {
    cleanup();
  });

  afterAll(() => {
    mswServer.close();
  });

  // ==== GETTING ====
  describe('when viewing the homepage.', () => {
    it('should render the component successfully.', async () => {
      const element = await screen.findByTestId('home-page-container');
      expect(element).toBeInTheDocument();
    });

    it('should render the correct number of launches.', async () => {
      await waitFor(async () => {
        expect(await screen.findAllByTestId('container-item')).toHaveLength(3);
      });
    });
  });
});
