import { render, screen, waitFor } from '@testing-library/react';
import Item from '.';

const mockDataItem = {
  flight_number: 5,
  mission_name: 'mock mission',
  launch_date_utc: '2006-03-24T22:30:00.000Z',
  rocket: {
    first_stage: {
      cores: [
        {
          core_serial: 'Merlin1A',
        },
      ],
    },
    second_stage: {
      payloads: [
        {
          payload_id: 'FalconSAT-2',
          payload_type: 'Satellite',
        },
      ],
    },
  },
  links: {
    mission_patch_small: 'https://images2.imgbox.com/3c/0e/T8iJcSN3_o.png',
  },
  launch_success: false,
  launch_failure_details: {
    time: 33,
    altitude: null,
    reason: 'merlin engine failure',
  },
};

test('renders the component successfully and displays correct data.', async () => {
  render(<Item dataItem={mockDataItem} />);

  expect(screen.getByTestId('card-header')).toHaveTextContent(/mock mission/);
  expect(screen.getByTestId('chip-failure')).toBeInTheDocument();
  await waitFor(() => {
    expect(screen.queryByLabelText('success')).toBeNull();
  });
  expect(screen.getByRole('img')).toBeInTheDocument();
});
