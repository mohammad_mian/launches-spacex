import Grid from '@mui/material/Grid';
import moment from 'moment';
import CardHeader from '@mui/material/CardHeader';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Chip from '@mui/material/Chip';
import Stack from '@mui/material/Stack';
import Image from 'next/image';
import { LaunchDataItem, Core, Payload } from '../List/types';

type ItemProps = {
  dataItem: LaunchDataItem;
};

const Item = (props: ItemProps) => {
  const {
    dataItem: { flight_number, mission_name, launch_date_utc, rocket, links, launch_success, launch_failure_details },
  } = props;
  const {
    first_stage: { cores },
    second_stage: { payloads },
  } = rocket;

  const allCoreSerials = cores.map((currentCore: Core) => currentCore.core_serial);
  const payloadsData = payloads.map((currentPayload: Payload) => {
    return {
      payload_id: currentPayload.payload_id,
      payload_type: currentPayload.payload_type,
    };
  });

  const { mission_patch_small } = links;

  return (
    <Grid item key={flight_number} xs={12} sm={12} md={4} data-testid="container-item">
      <Card>
        <CardHeader
          title={mission_name}
          subheader={moment.utc(launch_date_utc).format('DD/MM/YYYY')}
          titleTypographyProps={{ align: 'center' }}
          subheaderTypographyProps={{
            align: 'center',
          }}
          data-testid="card-header"
        />
        <CardContent>
          <Box
            sx={{
              display: 'flex',
              justifyContent: 'center',
              alignItems: 'baseline',
              mb: 2,
            }}
          >
            <Image src={mission_patch_small} alt={mission_name} width="200px" height="200px" />
          </Box>
          <ul>
            {allCoreSerials.map((core, i) => (
              <Typography component="li" variant="subtitle1" key={i}>
                {core}
              </Typography>
            ))}
          </ul>
          <ul>
            {payloadsData.map(({ payload_id, payload_type }) => (
              <div key={payload_id}>
                <Typography component="li" variant="subtitle1">
                  {payload_id}
                </Typography>
                <Typography component="li" variant="subtitle1">
                  {payload_type}
                </Typography>
              </div>
            ))}
          </ul>
          <Stack direction="row" spacing={1}>
            {launch_success ? (
              <Chip label="success" color="success" data-testid="chip-success" />
            ) : (
              <Chip
                label={`Failure: ${launch_failure_details.reason}`}
                color="error"
                variant="outlined"
                data-testid="chip-failure"
              />
            )}
          </Stack>
        </CardContent>
      </Card>
    </Grid>
  );
};

export default Item;
