import Item from '../Item';
import Container from '@mui/material/Container';
import Grid from '@mui/material/Grid';
import isEmpty from '../../utils/isEmpty';
import { LaunchDataResponse, LaunchDataItem } from './types';

type ListProps = {
  items: LaunchDataResponse;
};

const List = (props: ListProps) => {
  const { items } = props;
  return (
    <Container maxWidth="md" component="main" data-testid="container-list">
      <Grid container spacing={5} alignItems="flex-end">
        {!isEmpty(items) &&
          items.map((currentItem: LaunchDataItem) => (
            <Item key={currentItem.flight_number} dataItem={currentItem} data-testid="item" />
          ))}
      </Grid>
    </Container>
  );
};

export default List;
