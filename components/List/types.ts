export type Payload = {
  payload_id: string;
  payload_type: string;
};

export type Core = {
  core_serial: string;
};

export type Rocket = {
  first_stage: {
    cores: Core[];
  };
  second_stage: {
    payloads: Payload[];
  };
};

export type LaunchDataItem = {
  flight_number: number;
  mission_name: string;
  launch_date_utc: string;
  rocket: Rocket;
  links: {
    mission_patch_small: string;
  };
  launch_success: boolean;
  launch_failure_details: {
    reason: string;
  };
};

export type LaunchDataResponse = LaunchDataItem[];
