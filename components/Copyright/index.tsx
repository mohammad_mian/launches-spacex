import Typography from '@mui/material/Typography';
import Link from '@mui/material/Link';

type Props = {
  title: string;
};

const Copyright = (props: Props) => {
  const { title } = props;
  return (
    <Typography variant="body2" color="text.secondary" align="center" mt={5} data-testid="container">
      {'Copyright © '}
      <Link data-testid="title" color="inherit" href="https://www.avayler.com/">
        {title}
      </Link>{' '}
      {new Date().getFullYear()}
      {'.'}
    </Typography>
  );
};

export default Copyright;
