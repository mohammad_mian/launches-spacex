import { render, screen } from '@testing-library/react';
import Copyright from '.';

test('renders the component successfully.', () => {
  render(<Copyright title="Hello world" />);

  expect(screen.getByTestId('container')).toBeInTheDocument();
  expect(screen.getByTestId('title')).toHaveTextContent(/Hello world/);
});
