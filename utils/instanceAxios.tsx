import axios from 'axios';

const REQUEST_TIMEOUT_DURATION_MS = 10 * 1000;
export const baseURL = 'https://api.spacexdata.com/v3/launches';

export const instanceAxios = () => {
  // Instantiate axios using some default headers
  const axiosInstance = axios.create({
    baseURL,
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
    timeout: REQUEST_TIMEOUT_DURATION_MS,
  });

  return axiosInstance;
};
