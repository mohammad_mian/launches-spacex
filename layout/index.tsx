import Container from '@mui/material/Container';
import Typography from '@mui/material/Typography';
import { ReactNode } from 'react';
import AppBar from '../layout/AppBar';
import Footer from '../layout/Footer';

type LayoutProps = {
  children: ReactNode;
};

const Layout = (props: LayoutProps) => {
  const { children } = props;
  return (
    <>
      <AppBar />
      <Container disableGutters maxWidth="md" component="main" sx={{ pt: 8, pb: 6 }}>
        <Typography component="h1" variant="h2" align="center" color="text.primary" gutterBottom>
          SpaceX Data
        </Typography>
        <Typography variant="h5" align="center" color="text.secondary" component="p">
          We are not affiliated, associated, authorized, endorsed by, or in any way officially connected with Space
          Exploration Technologies Inc (SpaceX), or any of its subsidiaries or its affiliates. The names SpaceX as well
          as related names, marks, emblems and images are registered trademarks of their respective owners.
        </Typography>
      </Container>
      {children}
      <Footer />
    </>
  );
};

export default Layout;
