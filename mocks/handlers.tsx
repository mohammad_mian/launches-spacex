import { rest } from 'msw';
import { baseURL } from '../utils/instanceAxios';

const mockLaunchesData = [
  {
    flight_number: 5,
    mission_name: 'mock mission',
    launch_date_utc: '2006-03-24T22:30:00.000Z',
    rocket: {
      first_stage: {
        cores: [
          {
            core_serial: 'Merlin1A',
          },
        ],
      },
      second_stage: {
        payloads: [
          {
            payload_id: 'FalconSAT-2',
            payload_type: 'Satellite',
          },
        ],
      },
    },
    links: {
      mission_patch_small: 'https://images2.imgbox.com/3c/0e/T8iJcSN3_o.png',
    },
    launch_success: false,
    launch_failure_details: {
      time: 33,
      altitude: null,
      reason: 'merlin engine failure',
    },
  },
  {
    flight_number: 6,
    mission_name: 'mock mission 6',
    launch_date_utc: '2007-04-24T22:20:00.000Z',
    rocket: {
      first_stage: {
        cores: [
          {
            core_serial: 'mock 2',
          },
        ],
      },
      second_stage: {
        payloads: [
          {
            payload_id: 'mock-2',
            payload_type: 'Satellite mocks',
          },
        ],
      },
    },
    links: {
      mission_patch_small: 'https://images2.jukebox.com/3c/0e/T8iJcSN3_o.png',
    },
    launch_success: false,
    launch_failure_details: {
      time: 36,
      altitude: null,
      reason: 'merlin engine failure mocked',
    },
  },
  {
    flight_number: 7,
    mission_name: 'mock mission 7',
    launch_date_utc: '2006-05-24T22:10:00.000Z',
    rocket: {
      first_stage: {
        cores: [
          {
            core_serial: 'Merlin1A',
          },
        ],
      },
      second_stage: {
        payloads: [
          {
            payload_id: 'FalconSAT-2 mock',
            payload_type: 'Satellite mocked',
          },
        ],
      },
    },
    links: {
      mission_patch_small: 'https://images2.imgbox.com/3c/0e/T8iJcSN3_o.png',
    },
    launch_success: false,
    launch_failure_details: {
      time: 23,
      altitude: null,
      reason: 'merlin engine failure mocked',
    },
  },
];

export const handlers = [
  rest.get(baseURL, (req, res, ctx) => {
    return res(ctx.json(mockLaunchesData));
  }),
];
