import { useState, useEffect } from 'react';
import Head from 'next/head';
import Layout from '../layout';
import Backdrop from '@mui/material/Backdrop';
import CircularProgress from '@mui/material/CircularProgress';
import { instanceAxios } from '../utils/instanceAxios';
import List from '../components/List';
import { LaunchDataResponse } from '../components/List/types';

export default function Home() {
  const [isLoading, setLoading] = useState(false);
  const [data, setData] = useState(null);
  const [isError, setError] = useState({
    error: false,
    errorMessage: '',
  });

  useEffect(() => {
    setLoading(true);

    const fetchData = () => {
      const iAxios = instanceAxios();

      iAxios
        .get('/')
        .then((response) => {
          const launchDataItems = response.data as LaunchDataResponse;
          setData(launchDataItems.slice(0, 10));
        })
        .catch(() => {
          setError({
            error: true,
            errorMessage: 'Something went wrong. Pleae try again later.',
          });
        })
        .finally(() => {
          setLoading(false);
        });
    };
    fetchData();
  }, []);

  if (isLoading) {
    return (
      <Backdrop open={isLoading} sx={{ color: '#fff', zIndex: (theme) => theme.zIndex.drawer + 1 }}>
        <CircularProgress color="secondary" />
      </Backdrop>
    );
  }

  return (
    <div data-testid="home-page-container">
      <Head>
        <title>Launches Pairing</title>
        <meta name="description" content="Rocket Launches" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <main>
        <Layout>
          <List items={data} />
        </Layout>
      </main>
    </div>
  );
}
